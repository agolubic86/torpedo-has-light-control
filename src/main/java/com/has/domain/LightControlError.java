package com.has.domain;

/**
 * Created by torpedo on 01.02.18..
 */
public class LightControlError extends RuntimeException {

    public LightControlError(String errorMessage) {
        super(errorMessage);
    }

    public LightControlError(String errorMessage, Throwable th) {
        super(errorMessage, th);
    }
}
