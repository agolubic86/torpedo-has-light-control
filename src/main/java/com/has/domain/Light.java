package com.has.domain;

import com.has.constants.LightStatus;
import com.has.constants.Room;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * Created by torpedo on 01.12.17..
 */

@Document(collection = "light")
public class Light {

    public Light(){
        this.status = LightStatus.OFF;
    }

    public Light(Integer lightId, Room room){
        this.lightId = lightId;
        this.room = room;
        this.status = LightStatus.OFF;
    }

    public Light(Integer lightId, LightStatus status, Room room) {
        this.lightId = lightId;
        this.status = status;
        this.room = room;
    }

    @Id
    private String id;
    @NotNull(message = "LightId can not be null")
    private Integer lightId;
    private LightStatus status;
    @NotNull(message = "Room can not be null")
    private Room room;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getLightId() {
        return lightId;
    }

    public void setLightId(Integer lightId) {
        this.lightId = lightId;
    }

    public LightStatus getStatus() {
        return status;
    }

    public void setStatus(LightStatus status) {
        this.status = status;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Light: id=" + lightId
                + " status=" + status.name();
    }
}
