package com.has.mqttHelper;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by torpedo on 22.02.18..
 *
 */
@Component
@ConfigurationProperties("mqttTopic")
public class LightMqttTopicProperties {
    private String subscribe;
    private String publish;

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }
}
