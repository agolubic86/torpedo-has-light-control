package com.has;

import com.has.mqtt.MqttUtility;
import com.has.mqttHelper.LightMqttTopicProperties;
import com.has.mqttHelper.MqttLightCallback;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableDiscoveryClient
@EnableWebSecurity
@PropertySource({"classpath:/mqtt.properties", "classpath:/mongodb.properties", "classpath:/application.properties"})
public class TorpedoHasLightControlApplication {

	@Bean
	public MqttLightCallback mqttLightCallback() {
		return new MqttLightCallback();
	}

	@Bean
	public MqttUtility mqttUtility() {
		return new MqttUtility(mqttLightCallback());
	}

	public static void main(String[] args) {

		System.setProperty("spring.config.name", "light-server");
		ApplicationContext context = SpringApplication.run(TorpedoHasLightControlApplication.class, args);

		MqttUtility mqttUtility = context.getBean(MqttUtility.class);
		LightMqttTopicProperties topicProperties = context.getBean(LightMqttTopicProperties.class);
		mqttUtility.subscribe(topicProperties.getSubscribe());
	}
}

