package com.has.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.has.constants.LightStatus;
import com.has.constants.Room;
import com.has.domain.Light;
import com.has.domain.LightControlError;
import com.has.mqtt.MqttUtility;
import com.has.mqttHelper.LightMqttTopicProperties;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.model.MappingException;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by torpedo on 30.01.18..
 */
@Service
public class LightManagerImpl implements LightManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(LightManagerImpl.class);

    public static final String LIGHT_ID_PARAM = "lightId";
    public static final String ROOM_PARAM = "room";
    public static final String STATUS_PARAM = "status";

    @Autowired
    private LightMqttTopicProperties topicProperties;

    @Autowired
    private MqttUtility mqttUtility;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Light registerLight(Light light) {
        try {
            LOGGER.info("Registering new Light LIGHT={}", light);
            if (mongoTemplate.exists(Query.query(Criteria.where(LIGHT_ID_PARAM).is(light.getLightId()).and(ROOM_PARAM).is(light.getRoom())), Light.class))
                throw new LightControlError(String.format("Light with id = %s in room = %s already registered.", light.getLightId(), light.getRoom().name()));

            mongoTemplate.insert(light);

            LOGGER.info("Registering new Light LIGHT={} - SUCCESSFUL", light);

            return light;
        }catch (MongoException me) {
            throw new LightControlError("Error registering new Light.", me);
        }
    }

    @Override
    public Light changeLightStatus(final Integer lightId, final Integer roomId, final LightStatus status) {
        try {
            Room room = Room.getByValue(roomId);
            LOGGER.info("Changing light status LIGHT_ID_PARAM={}, ROOM_ID={}, STATUS={}", lightId, room, status);

            Map<String, Object> params = new HashMap<>();
            params.put(LIGHT_ID_PARAM, lightId);
            params.put(ROOM_PARAM, room);
            params.put(STATUS_PARAM, status);

            Light light = saveToDatabase(params);
            if(light == null) {
                throw new LightControlError("No light found to update.");
            }
            sendLedControlMessage(light);
            LOGGER.info("Changing light status LIGHT_ID_PARAM={}, ROOM_ID={}, STATUS={} - SUCCESSFUL", lightId, room, status);

            return light;
        } catch (MongoException e) {
            throw new LightControlError("Error updating status.", e);
        } catch (JsonProcessingException e) {
            throw new LightControlError("Error mapping object as JSON.", e);
        }
    }

    @Override
    public Light findLight(final Integer roomId, final Integer lightId) {
        Room room = Room.getByValue(roomId);
        LOGGER.info("Finding light by ROOM={} and LIGHT_ID_PARAM={}", room, lightId);
        return mongoTemplate.findOne(Query.query(Criteria.where(LIGHT_ID_PARAM).is(lightId).and(ROOM_PARAM).is(room)), Light.class);
    }

    @Override
    public List<Light> findLightsByRoomId(final Integer roomId) {
        Room room = Room.getByValue(roomId);
        LOGGER.info("Finding lights in ROOM={}", room);
        return mongoTemplate.find(Query.query(Criteria.where(ROOM_PARAM).is(room)), Light.class);
    }

    @Override
    public void deleteLight(Integer roomId, Integer lightId) {
        Room room = Room.getByValue(roomId);
        LOGGER.info("Deleting light in ROOM={} and LIGHT_ID_PARAM={}", room, lightId);
        try {
            mongoTemplate.remove(Query.query(Criteria.where(LIGHT_ID_PARAM).is(lightId).and(ROOM_PARAM).is(room)), Light.class);
        }catch (MongoException me) {
            String msg = String.format("Error deleting light with id=%s in room=%s", lightId, room);
            throw new LightControlError(msg, me);
        }catch (MappingException mpe) {
            String msg = String.format("Error deleting light with id=%s in room=%s", lightId, room);
            throw new LightControlError(msg, mpe);
        }
    }

    @Override
    public List<Light> fetchLights() {
        return mongoTemplate.findAll(Light.class);
    }

    private Light saveToDatabase(Map<String, Object> params) {
        LOGGER.info("Saving light to database. Params={}", params);
        Query query = new Query();
        query.addCriteria(Criteria.where(LIGHT_ID_PARAM).is(params.get(LIGHT_ID_PARAM)).and(ROOM_PARAM).is(params.get(ROOM_PARAM)));

        Update update = new Update();
        update.set(STATUS_PARAM, params.get(STATUS_PARAM));

        FindAndModifyOptions options = new FindAndModifyOptions();
        options.upsert(false).returnNew(true);

        Light light = mongoTemplate.findAndModify(query, update, options, Light.class);
        LOGGER.info("Saving light to database. Params={}", params);

        return light;
    }

    private void sendLedControlMessage(Light light) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonObject payload = new JsonObject();
        payload.addProperty("light", mapper.writeValueAsString(light));

        mqttUtility.publish(topicProperties.getPublish(), payload);

    }
}
