package com.has.services;


import com.has.constants.LightStatus;
import com.has.domain.Light;

import java.util.List;

/**
 * Created by torpedo on 30.01.18..
 */
public interface LightManager {

    Light changeLightStatus(Integer lightId, Integer roomId, LightStatus status);

    Light findLight(Integer roomId, Integer lightId);

    List<Light> fetchLights();

    Light registerLight(Light light);

    List<Light> findLightsByRoomId(Integer roomId);

    void deleteLight(Integer roomId, Integer lightId);
}
