package com.has.controller;

import com.has.constants.LightStatus;
import com.has.constants.Room;
import com.has.domain.Light;
import com.has.services.LightManager;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by torpedo on 01.12.17..
 */

@RestController
public class LightController {

    @Value("${build.version}")
    private String buildVersion;

    @Value("${build.timestamp}")
    private String buildTimestamp;

    @Autowired
    private LightManager lightManager;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "LIGHT SERVICE. Version: " + buildVersion + "- Build@" + buildTimestamp;
    }

    @RequestMapping(value = "/light/{lightId}/room/{roomId}/", method = RequestMethod.POST)
    public Light registerNewLight(@PathVariable("roomId") final Integer roomId, @PathVariable("lightId") final Integer lightId) {
        Light light = new Light(lightId, Room.getByValue(roomId));
        return lightManager.registerLight(light);
    }

    @RequestMapping(value = "/light/{lightId}/room/{roomId}", method = RequestMethod.GET)
    public Light findLight(@PathVariable("lightId") final Integer lightId, @PathVariable("roomId") final Integer roomId) {
        return lightManager.findLight(roomId, lightId);
    }

    @RequestMapping(value = "/light/{lightId}/room/{roomId}", method = RequestMethod.DELETE)
    public void deleteLightById(@PathVariable("lightId") final Integer lightId, @PathVariable("roomId") final Integer roomId) {
        lightManager.deleteLight(roomId, lightId);
    }

    @RequestMapping(value = "light/room/{roomId}", method = RequestMethod.GET)
    public List<Light> findLightByRoomId(@PathVariable("roomId") final Integer roomId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return lightManager.findLightsByRoomId(roomId);
    }

    @RequestMapping(value = "/light", method = RequestMethod.GET)
    public List<Light> fetchLightList() {
        return lightManager.fetchLights();
    }

    @RequestMapping(value = "/light/{lightId}/room/{roomId}/on", method = RequestMethod.PUT)
    public Light lightControlOn(@PathVariable("roomId") final Integer roomId, @PathVariable("lightId") final Integer lightId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return lightManager.changeLightStatus(lightId, roomId, LightStatus.ON);
    }

    @RequestMapping(value = "/light/{lightId}/room/{roomId}/off", method = RequestMethod.PUT)
    public Light lightControlOff(@PathVariable("roomId") final Integer roomId, @PathVariable("lightId") final Integer lightId) {
        Assert.notNull(Room.getByValue(roomId), "Invalid room id");
        return lightManager.changeLightStatus(lightId, roomId, LightStatus.OFF);
    }


}
