package com.has.constants;

import java.security.InvalidParameterException;

/**
 * Created by torpedo on 30.01.18..
 */
public enum Room {
    LIVING_ROOM(0),
    BEDROOM(1),
    KITCHEN(2),
    LOGGIA(3),
    PANTRY(4),
    MAIN_HALL(5),
    HALL(6),
    BATHROOM(7),
    TOILET(8);

    private Integer roomId;

    Room(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getValue() {return roomId;}

    public static Room getByValue(Integer roomId) {
        for (Room room : Room.values()) {
            if(room.getValue() == roomId)
                return room;
        }

        throw new InvalidParameterException("No room with id=" + roomId + " found");
    }

}
