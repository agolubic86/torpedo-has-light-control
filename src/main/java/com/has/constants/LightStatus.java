package com.has.constants;

/**
 * Created by torpedo on 30.01.18..
 */
public enum LightStatus {
    ON(1),
    OFF(0);

    private final int statusValue;

    LightStatus(int statusValue) { this.statusValue = statusValue; }
    public int getValue() { return statusValue; }
}
